<?php

namespace Drupal\copysafe_video_protection\Plugin\Filter;

use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to copy safe video protection.
 *
 * The attributes in the annotation show examples of allowing all attributes
 * by only having the attribute name, or allowing a fixed list of values, or
 * allowing a value with a wildcard prefix.
 *
 * @Filter(
 *   id = "copysafevideo_protection",
 *   title = @Translation("Enable CopySafeVideo tag replacement"),
 *   description = @Translation("Enables CopySafeVideo tag replacement."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 *   settings = {
 *   },
 *   weight = -10
 * )
 */
class FilterCopySafeVideo extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (preg_match_all('/\[copysafe_video_protection[^\]]+\]/i', $text, $matches)) {
      global $base_url;
      $settings = [];

      $video_config = \Drupal::config('copysafe_video_protection.settings');

      foreach ($matches[0] as $matched) {
        $img = [];
        preg_match_all('/(name|height|width|filename|prints_allowed|print_anywhere|allow_capture|allow_remote|background)=([\'|"][^\']*[\'|"])/i', $matched, $img);
        $atts = [];

        for ($i = 0; $i < count($img[1]); $i++) {
          $atts[$img[1][$i]] = $img[2][$i];
        }

        static $embed_id;

        if (!isset($embed_id)) {
          $embed_id = 0;
        }

        $csp_id = "copysafe_video_protection_id" . $embed_id;
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['mode'] = !empty($video_config->get('mode')) ? $video_config->get('mode') : '';
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['artisbrowser_min_version'] = !empty($video_config->get('artisbrowser_min_version')) ? $video_config->get('artisbrowser_min_version') : '27.11';
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['bgheight'] = (isset($atts['height'])) ? str_replace("'", "", $atts['height']) : '';
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['bgwidth'] = (isset($atts['width'])) ? str_replace("'", "", $atts['width']) : '';
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['name'] = str_replace("'", "", $atts['name']);
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['allow_remote'] = (isset($atts['allow_remote'])) ? str_replace("'", "", $atts['allow_remote']) : 0;

        $default = [
          'asps'    => 'asps',
          'firefox' => 'firefox',
          'chrome'  => 'chrome',
        ];
        $browser = !empty($video_config->get('browser')) ? $video_config->get('browser') : $default;
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['name'] = str_replace("'", "", $atts["name"]);
        $filename = str_replace("'", "", $atts["name"]);

        $folder = !empty($video_config->get('uploadfolder')) ? $video_config->get('uploadfolder') : 'upload_folder/copysafe_video_protection';
        $upload_folder = PublicStream::basePath() . "/" . $folder . "/";

        if (!file_exists($upload_folder . $filename)) {
          return "<div style='padding:5px 10px;background-color:#fffbcc'><strong>File($filename) don't exist</strong></div>";
        }

        $settings['copysafe_video_protection']['embed_options'][$csp_id]['asps'] = "";
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['chrome'] = "";
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['firefox'] = "";

        if (isset($browser['chrome'])) {
          if ($browser['chrome'] === "chrome") {
            $settings['copysafe_video_protection']['embed_options'][$csp_id]['chrome'] = 1;
          }
        }
        if (isset($browser['firefox'])) {
          if ($browser['firefox'] === "firefox") {
            $settings['copysafe_video_protection']['embed_options'][$csp_id]['firefox'] = 1;
          }
        }

        if (isset($browser['asps'])) {
          if ($browser['asps'] === "asps") {
            $settings['copysafe_video_protection']['embed_options'][$csp_id]['asps'] = 1;
          }
        }

        $settings['copysafe_video_protection']['embed_options'][$csp_id]['plugin_url'] = $base_url . '/' . drupal_get_path('module', 'copysafe_video_protection') . '/';
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['plugin_path'] = $base_url . '/' . drupal_get_path('module', 'copysafe_video_protection') . '/';
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['upload_path'] = $base_url . '/' . $upload_folder;
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['upload_url'] = $base_url . '/' . $upload_folder;

        static $inc;

        if (!isset($inc)) {
          $inc = 0;
        }
        $outputid = "output" . $inc;
        $settings['copysafe_video_protection']['embed_options'][$csp_id]['outputid'] = (isset($outputid)) ? $outputid : '';

        $output = <<<html
            <div id = $outputid>
            </div>
html;
        $text = $this->strReplaceOnce($matched, $output, $text);
        $embed_id++;
        $inc = $inc + 1;
      }

      $result = new FilterProcessResult($text);
      $result->setAttachments([
        'library'        => [
          'copysafe_video_protection/copysafe_video_protection',
        ],
        'drupalSettings' => $settings,
      ]);

      return $result;
    }

    return new FilterProcessResult($text);
  }

  /**
   * Function() for CopySafe Video String Replacement.
   */
  public function strReplaceOnce($str_pattern, $str_replacement, $string) {

    if (strpos($string, $str_pattern) !== FALSE) {
      return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
    }

    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = '<p>' . $this->t('HTML header tags generated by Microsoft Office are stripped') . '</p>';
    return $output;
  }

}
