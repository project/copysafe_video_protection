<?php

namespace Drupal\copysafe_video_protection\Controller;

use Drupal\Core\Entity\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

/**
 * CopySafeVideoController class.
 */
class CopySafeVideoController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The CopySafeVideoController constructor.
   *
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity manager.
   */
  public function __construct(FormBuilder $formBuilder, EntityManagerInterface $entity_manager) {
    $this->formBuilder = $formBuilder;
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Drupal service container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder'),
      $container->get('entity.manager')
    );
  }

  /**
   * Callback for opening the modal form.
   */
  public function openEmbedOptionsModalForm(Request $request, $fid = NULL) {
    $response = new AjaxResponse();

    $fid = (int) $fid;
    if (is_null($fid) || !is_numeric($fid)) {
      return ['#makup' => ''];
    }

    /** @var \Drupal\file\Entity\File $file */
    $file = $this->entityManager->getStorage('file')->load($fid);

    // Get the modal form using the form builder.
    $modal_form = $this->formBuilder->getForm('Drupal\copysafe_video_protection\Form\CopySafeVideoClass', ['fid' => (int) $fid]);

    // Add an AJAX command to open a modal dialog with the form as the content.
    $response->addCommand(new OpenModalDialogCommand($this->t('File embed options for @name', ['@name' => $file->getFilename()]), $modal_form, ['width' => '800']));

    return $response;
  }

}
