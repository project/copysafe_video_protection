<?php

namespace Drupal\copysafe_video_protection\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure copy safe video configuration for this site.
 *
 * @internal
 */
class CopySafeVideoConfig extends ConfigFormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a Copysafe Video settings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity manager.
   * @param \Drupal\Core\Database\Connection $database
   *   Database object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityManagerInterface $entity_manager, Connection $database) {
    $this->configFactory = $config_factory;
    $this->entityManager = $entity_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'copysafe_video_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['copysafe_video_protection.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('copysafe_video_protection.settings');

    $upload_directory = !empty($settings->get('uploadfolder')) ? $settings->get('uploadfolder') : 'upload_folder/copysafe_video_protection';
    $upload_location = 'public://' . $upload_directory;

    $form['custom_field_name']['image'] = [
      '#type'              => 'managed_file',
      '#title'             => $this->t('File Upload'),
      '#upload_validators' => [
        'file_validate_extensions' => ['class'],
      ],
      '#upload_location'   => $upload_location,
      '#description'       => $this->t("Only .class files can be uploaded here."),
    ];

    $result = db_query("SELECT * FROM {file_managed}")->fetchAll();

    $header = [];
    $header[] = ['data' => 'File', 'field' => 'filename'];
    $header[] = ['data' => 'Size', 'field' => 'filesize'];
    $header[] = ['data' => 'Date', 'field' => 'timestamp'];
    $header[] = ['data' => 'Delete'];
    $header[] = ['data' => 'Insert'];

    $data = [];
    foreach ($result as $record) {
      if (strpos($record->uri, $upload_location) !== FALSE) {
        $size = round($record->filesize / 1024, 2) . "KB";
        $date = date('d-m-Y H:i:s', $record->created);
        $delete_url = Url::fromRoute('copysafe_video_protection.copysafe_video_file_delete', ['fid' => $record->fid])
          ->toString();
        $del_link_html = new FormattableMarkup('<a href="@dellinkpath" class="button">@name</a>', [
          '@dellinkpath' => $delete_url,
          '@name'        => 'Delete',
        ]);
        $embed_url = Url::fromRoute('copysafe_video_protection.open_embed_options_form', ['fid' => $record->fid])
          ->toString();
        $embed_link_html = new FormattableMarkup('<a href="@linkpath" class="use-ajax button">@name</a>', [
          '@linkpath' => $embed_url,
          '@name'     => 'Embed options',
        ]);

        $data[$record->fid] = [
          $record->filename,
          $size,
          $date,
          $del_link_html,
          $embed_link_html,
        ];
      }
    }

    $form['html'] = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $data,
      '#empty'  => $this->t('No files available.'),
      '#weight' => 9999,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('image'))) {
      $form_state->setErrorByName('image', $this->t('Please upload a class file.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    /** @var \Drupal\file\Entity\File $file */
    $strip_file = $this->entityManager->getStorage('file')
      ->load($values['image']['0']);
    if ($strip_file->isTemporary()) {
      $strip_file->setPermanent();
      $strip_file->save();
    }

    parent::submitForm($form, $form_state);
  }

}
