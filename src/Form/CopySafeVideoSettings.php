<?php

namespace Drupal\copysafe_video_protection\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Copysafe Video settings for this site.
 *
 * @internal
 */
class CopySafeVideoSettings extends ConfigFormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a Copysafe Video settings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['copysafe_video_protection.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'copysafe_video_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('copysafe_video_protection.settings');

    $form['uploadfolder'] = [
      '#type'          => 'textfield',
      '#default_value' => !empty($settings->get('uploadfolder')) ? $settings->get('uploadfolder') : 'upload_folder/copysafe_video_protection',
      '#title'         => $this->t('Upload Folder'),
      '#field_prefix'  => $this->t('sites/default/files/'),
      '#required'      => TRUE,
    ];

    $form['mode'] = [
      '#type'          => 'select',
      '#default_value' => !empty($settings->get('mode')) ? $settings->get('mode') : '',
      '#options'       => [
        'demo'     => $this->t('Demo Mode'),
        'licensed' => $this->t('Licensed'),
        'debug'    => $this->t('Debugging Mode'),
      ],
      '#title'         => $this->t('Mode'),
    ];

    $form['language'] = [
      '#type'          => 'select',
      '#default_value' => !empty($settings->get('language')) ? $settings->get('language') : '',
      '#options'       => [
        '0c01' => $this->t('Arabic'),
        '0004' => $this->t('Chinese (simplified)'),
        '0404' => $this->t('Chinese (traditional)'),
        '041a' => $this->t('Croatian'),
        '0405' => $this->t('Czech)'),
        '0413' => $this->t('Dutch'),
        ''     => $this->t('English'),
        '0464' => $this->t('Filipino'),
        '000c' => $this->t('French'),
        '0007' => $this->t('German'),
        '0408' => $this->t('Greek'),
        '040d' => $this->t('Hebrew'),
        '0439' => $this->t('Hindi'),
        '000e' => $this->t('Hungarian'),
        '0421' => $this->t('Indonesian'),
        '0410' => $this->t('Italian'),
        '0411' => $this->t('Japanese'),
        '0412' => $this->t('Korean'),
        '043e' => $this->t('Malay'),
        '0415' => $this->t('Polish'),
        '0416' => $this->t('Portuguese (BR)'),
        '0816' => $this->t('Portuguese (PT)'),
        '0419' => $this->t('Russian'),
        '0c0a' => $this->t('Spanish'),
        '041e' => $this->t('Thai'),
        '041f' => $this->t('Turkish'),
        '002a' => $this->t('Vietnamese'),
      ],
      '#title'         => $this->t('Language'),
    ];

    $form['pagecolor'] = [
      '#type'          => 'textfield',
      '#default_value' => !empty($settings->get('pagecolor')) ? $settings->get('pagecolor') : 'CCCCCC',
      '#title'         => $this->t('Page Color'),
    ];

    $options = [
      'asps'    => $this->t('Allow ArtisBrowser'),
      'firefox' => $this->t('Allow Firefox (for test only)'),
      'chrome'  => $this->t('Allow Chrome (for test only)'),
    ];

    $form['artisbrowser_min_version'] = [
      '#type'          => 'textfield',
      '#default_value' => !empty($settings->get('artisbrowser_min_version')) ? $settings->get('artisbrowser_min_version') : '27.11',
      '#title'         => $this->t('ArtisBrowser minimum version'),
      '#description'   => $this->t('The ArtisBrowser minimum version should have 2 parts like 27.11  !'),
    ];

    $form['browser'] = [
      '#title'         => $this->t('Select Browsers'),
      '#type'          => 'checkboxes',
      '#default_value' => !empty($settings->get('browser')) ? $settings->get('browser') : '',
      '#options'       => $options,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $min_version = $form_state->getValue('artisbrowser_min_version');
    $min_version_parts = explode('.', $min_version);
    if (count($min_version_parts) > 2 || !is_numeric($min_version_parts[0]) || !is_numeric($min_version_parts[1])) {
      $form_state->setErrorByName('artisbrowser_min_version', $this->t('The ArtisBrowser minimum version should be a valid version number with 2 parts like 27.11!'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('copysafe_video_protection.settings')
      ->set('uploadfolder', $form_state->getValue('uploadfolder'))
      ->set('mode', $form_state->getValue('mode'))
      ->set('language', $form_state->getValue('language'))
      ->set('pagecolor', $form_state->getValue('pagecolor'))
      ->set('browser', $form_state->getValue('browser'))
      ->set('artisbrowser_min_version', $form_state->getValue('artisbrowser_min_version'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
