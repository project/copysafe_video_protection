<?php

namespace Drupal\copysafe_video_protection\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure copy safe video class configuration for this site.
 *
 * @internal
 */
class CopySafeVideoClass extends FormBase {

  /**
   * ID of the file to generate copy safe script.
   *
   * @var int
   */
  protected $fid;

  /**
   * The entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityManagerInterface $entity_manager, ConfigFactoryInterface $config_factory) {
    $this->entityManager = $entity_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'copysafe_video_class';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['copysafe_video_protection.class_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $fid = NULL) {
    $settings = $this->config('copysafe_video_protection.class_settings');

    $form['#prefix'] = '<div id="copysafe-class-config">';
    $form['#suffix'] = '</div>';

    if (empty($fid) || !is_numeric($fid['fid'])) {
      return RedirectResponse::create('/admin/config/copysafe/copysafe_video_protection', RedirectResponse::HTTP_MOVED_PERMANENTLY);
    }
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->entityManager->getStorage('file')->load($fid['fid']);
    if (empty($file)) {
      return RedirectResponse::create('/admin/config/copysafe/copysafe_video_protection', RedirectResponse::HTTP_MOVED_PERMANENTLY);
    }

    $form['filename'] = [
      '#type'          => 'textfield',
      '#default_value' => $file->getFilename(),
      '#title'         => $this->t('File Name'),
      '#disabled'      => TRUE,
    ];

    $form['width'] = [
      '#type'          => 'textfield',
      '#default_value' => !empty($settings->get('width')) ? $settings->get('width') : '',
      '#title'         => $this->t('Viewer Width'),
      '#size'          => 20,
      '#field_suffix'  => $this->t('(in pixels - leave blank to use filename settings)'),
    ];

    $form['height'] = [
      '#type'          => 'textfield',
      '#default_value' => !empty($settings->get('height')) ? $settings->get('height') : '',
      '#title'         => $this->t('Viewer Height'),
      '#size'          => 20,
      '#field_suffix'  => $this->t('(in pixels)'),
    ];

    $form['allow_remote'] = [
      '#type'          => 'checkbox',
      '#default_value' => !empty($settings->get('allow_remote')) ? $settings->get('allow_remote') : '',
      '#title'         => $this->t('Allow Remote'),
    ];

    $form['actions'] = ['#type' => 'actions'];

    $form['actions']['insertcode'] = [
      '#type'       => 'submit',
      '#value'      => $this->t('Generate CopySafe Video Script'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'edit-insertcode',
        ],
      ],
      '#ajax'       => [
        'callback' => [$this, 'submitClassFormAjax'],
        'event'    => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory
      ->getEditable('copysafe_video_protection.class_settings');

    $config->set('filename', $form_state->getValue('filename'))
      ->set('width', $form_state->getValue('width'))
      ->set('height', $form_state->getValue('height'))
      ->set('allow_remote', $form_state->getValue('allow_remote'))
      ->save();
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitClassFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#copysafe-class-config', $form));
    }
    else {
      $values = $form_state->getValues();

      $data = [
        "name='" . $values['filename'] . "'",
        "bgwidth='" . $values['width'] . "'",
        "bgheight='" . $values['height'] . "'",
        $values['allow_remote'] !== 0 ? "allow_remote='1'" : "allow_remote='0'",
      ];

      $copysafe_script = '<div id="copysafe-script-wrap">[copysafe_video_protection ' . implode(' ', $data) . ' ]</div>
      <br />';

      $response->addCommand(new OpenModalDialogCommand("CopySafe Video Script for " . $values['filename'], $copysafe_script, ['width' => 800]));
    }

    return $response;
  }

}
