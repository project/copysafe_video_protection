<?php

namespace Drupal\copysafe_video_protection\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Delete copy safe file for this site.
 *
 * @internal
 */
class CopySafeVideoFileDelete extends ConfirmFormBase {

  /**
   * ID of the file to delete.
   *
   * @var int
   */
  protected $fid;

  /**
   * The entity Manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a Copysafe Pdf Delete  object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   Entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'copysafe_video_file_delete';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $fid = NULL) {
    $this->fid = $fid = (int) $fid;

    if (is_null($this->fid) || !is_numeric($this->fid)) {
      return RedirectResponse::create('/admin/config/copysafe/copysafe_video_protection', RedirectResponse::HTTP_MOVED_PERMANENTLY);
    }
    else {
      /** @var \Drupal\file\Entity\File $file */
      $file = $this->entityManager->getStorage('file')->load($this->fid);
      if (empty($file)) {
        return RedirectResponse::create('/admin/config/copysafe/copysafe_video_protection', RedirectResponse::HTTP_MOVED_PERMANENTLY);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $storage = $this->entityManager->getStorage('file');
    $entity = $storage->load($this->fid);
    $storage->delete([$entity]);
    $form_state->setRedirect('copysafe_video_protection.copysafe_video');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('copysafe_video_protection.copysafe_video');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    if (is_null($this->fid) || !is_numeric($this->fid)) {
      return '';
    }
    else {
      $file = File::load($this->fid);
      if (!empty($file)) {
        return $this->t('Are you sure that you want to delete %id?', ['%id' => $file->getFilename()]);
      }
    }

  }

}
