(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * TestCSS function.
   * @param {object} prop
   *   Prop.
   * @return {boolean}
   *   Is prop in style.
   */
  function testCSS(prop) {
    return prop in document.documentElement.style;
  }

  /**
   * Function to get browser version.
   * @return {number[]}
   *   Version number.
   */
  function getArtisBrowserVersion() {
    const uMatch = navigator.userAgent.match(/ArtisBrowser\/(.*)$/);

    let ffVersion = '';
    if (uMatch && uMatch.length > 1) {
      ffVersion = [uMatch[1].split(' ')[0]];
    }
    const AC = [0, 0];
    let ix1 = 0;
    let ix2 = 0;
    if (ffVersion.length > 0) {
      ffVersion = String(ffVersion);
      ix1 = ffVersion.indexOf('.');
      ix2 = ffVersion.indexOf('.', ix1 + 1);
      if (ix1 !== -1 && ix2 !== -1) {
        AC[0] = parseInt(ffVersion.slice(0, ix1), 10);
        AC[1] = parseInt(ffVersion.slice(ix1 + 1, ix2), 10);
      }
    }
    return AC;
  }

  /**
   * Compares two software version numbers (e.g. "1.7.1")
   *
   * @param {string} v1
   *   Version V1.
   * @param {string} v2
   *  Version V2.
   * @param {array} options
   *   Options.
   * @return {number}
   * <ul>
   *    <li>0 if the versions are equal</li>
   *    <li>a negative integer iff v1 < v2</li>
   *    <li>a positive integer iff v1 > v2</li>
   *    <li>NaN if either version string is in the wrong format</li>
   * </ul>
   */
  function versionCompare(v1, v2, options) {
    const lexicographical = options && options.lexicographical;
    const zeroExtend = options && options.zeroExtend;

    let v1parts = v1.split('.');
    let v2parts = v2.split('.');

    function isValidPart(x) {
      return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
    }

    if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
      return NaN;
    }

    if (zeroExtend) {
      while (v1parts.length < v2parts.length) {
        v1parts.push('0');
      }
      while (v2parts.length < v1parts.length) {
        v2parts.push('0');
      }
    }

    if (!lexicographical) {
      v1parts = v1parts.map(Number);
      v2parts = v2parts.map(Number);
    }

    for (let i = 0; i < v1parts.length; ++i) {
      if (v2parts.length === i) {
        return 1;
      }
      if (v1parts[i] === v2parts[i]) {
        i += 1;
      }
      else if (v1parts[i] > v2parts[i]) {
        return 1;
      }
      else {
        return -1;
      }
    }

    if (v1parts.length !== v2parts.length) {
      return -1;
    }
    return 0;
  }

  /**
   *  Escape Html function.
   *
   * @param {string} unsafe
   *   Params.
   * @return {*}
   *   Safe string.
   */
  function escapeHtml(unsafe) {
    return unsafe
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#039;');
  }

  /**
   * The copysafe-insert functions.
   *
   * @param {string} wpcsp_plugin_url
   *   Wpcsp_plugin_url.
   * @param {string} output
   *   Output.
   * @param {boolean} m_bpDebugging
   *   M_bpDebugging.
   * @param {string} m_szPlugin
   *   M_szPlugin.
   * @param {string} m_bpWidth
   *   M_bpWidth.
   * @param {string} m_bpHeight
   *   M_bpHeight.
   * @param {string} m_szImageFolder
   *   M_szImageFolder.
   * @param {string} m_szClassName
   *   M_szClassName.
   * @param {string} m_bpAllowRemote
   *   M_bpAllowRemote.
   */
  function insertcopysafe_video_protection(wpcsp_plugin_url, output, m_bpDebugging, m_szPlugin, m_bpWidth, m_bpHeight, m_szImageFolder, m_szClassName, m_bpAllowRemote) {
    var textarea = 'ta' + output;
    var string = '<embed id="npCopySafeVideo" type="application/x-copysafe-video-plugin" width="' + m_bpWidth + '" height="' + m_bpHeight + '" src="' + m_szImageFolder + m_szClassName + '" remote="' + m_bpAllowRemote + '" />';

    if (m_bpDebugging === true) {
      jQuery('#' + output).append("<textarea rows='27' cols='80' id= '" + textarea + "'>");
      if ((m_szPlugin === 'DLL')) {
        jQuery('#' + textarea).append(escapeHtml(string));
      }
      jQuery('#' + output).append('</textarea>');
    }
    else {
      if ((m_szPlugin === 'DLL')) {
        jQuery('#' + output).append(string);
      }
    }
  }

  Drupal.behaviors.copysafe_video_protection_embed = {
    attach: function (context) {
      for (let val in drupalSettings.copysafe_video_protection.embed_options) {
        const wpcsp_plugin_url = drupalSettings.copysafe_video_protection.embed_options[val]['plugin_url'];
        let m_bpDebugging = false;
        const m_szMode = drupalSettings.copysafe_video_protection.embed_options[val]['mode'];
        const m_szClassName = drupalSettings.copysafe_video_protection.embed_options[val]['name'];
        const m_szImageFolder = drupalSettings.copysafe_video_protection.embed_options[val]['upload_url'];
        const m_bpChrome = drupalSettings.copysafe_video_protection.embed_options[val]['chrome'];
        const m_bpFx = drupalSettings.copysafe_video_protection.embed_options[val]['firefox'];
        const m_bpASPS = drupalSettings.copysafe_video_protection.embed_options[val]['asps'];
        const m_min_versionASPS = drupalSettings.copysafe_video_protection.embed_options[val]['artisbrowser_min_version'];
        const m_bpHeight = drupalSettings.copysafe_video_protection.embed_options[val]['bgheight'];
        const m_bpWidth = drupalSettings.copysafe_video_protection.embed_options[val]['bgwidth'];
        const m_bpAllowRemote = drupalSettings.copysafe_video_protection.embed_options[val]['allow_remote'];
        const m_szLocation = document.location.href.replace(/&/g, '%26');
        const m_szDownloadNo = wpcsp_plugin_url + 'download_no.html';
        const m_szDownload = wpcsp_plugin_url + 'download.html?ref=' + m_szLocation;
        const m_szAgent = navigator.userAgent.toLowerCase();
        let m_szPlugin = '';
        const m_bWindows = m_szAgent.indexOf('windows nt') !== -1;
        const m_bFx3 =
          m_szAgent.indexOf('firefox/3.') !== -1 &&
          m_szAgent.indexOf('flock') === -1 &&
          m_szAgent.indexOf('navigator') === -1;
        const m_bFx4 =
          m_szAgent.indexOf('firefox/4.') !== -1 &&
          m_szAgent.indexOf('flock') === -1 &&
          m_szAgent.indexOf('navigator') === -1;
        const m_bFirefox =
          m_szAgent.indexOf('firefox') !== -1 &&
          testCSS('MozBoxSizing') &&
          !m_bFx3 &&
          !m_bFx4 &&
          m_bpFx;
        const m_bChrome = m_szAgent.indexOf('chrome') !== -1 && m_bpChrome;
        const m_bASPS =
          (m_szAgent.indexOf('artisreader') !== -1 ||
            m_szAgent.indexOf('artisbrowser') !== -1) &&
          m_bpASPS;
        const m_bNetscape = m_bChrome || m_bFirefox || m_bASPS;

        if (m_bWindows && m_bNetscape) {
          m_szPlugin = 'DLL';
        }

        if (m_szMode === 'debug') {
          m_bpDebugging = true;
        }

        // In case it's windows and browser not artisbrowser.
        if ((m_bWindows) && !(m_bASPS) && !(m_bpDebugging)) {
          window.location = unescape(m_szDownload);
          document.MM_returnValue = false;
        }

        // Not windows.
        if (!m_bWindows) {
          window.location = unescape(m_szDownloadNo);
          document.MM_returnValue = false;
        }

        // Version check.
        if (m_bWindows && m_bASPS) {
          // Get the artisBrowser version.
          const arVersion = getArtisBrowserVersion();
          const szNumeric = arVersion[0] + '.' + arVersion[1];
          if (versionCompare(szNumeric, m_min_versionASPS) < 0) {
            window.location = unescape(m_szDownload);
            document.MM_returnValue = false;
          }
        }

        const output = drupalSettings.copysafe_video_protection.embed_options[val]['outputid'];

        if (m_szMode === 'licensed' || m_szMode === 'debug') {
          insertcopysafe_video_protection(wpcsp_plugin_url, output, m_bpDebugging, m_szPlugin, m_bpWidth, m_bpHeight, m_szImageFolder, m_szClassName, m_bpAllowRemote);
        }
        else {
          document.getElementById(output).innerHTML = "<img src='" + wpcsp_plugin_url + "images/demo_placeholder.jpg' border='0' alt='Demo mode'>";
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
