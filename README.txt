README.md for CopySafe Video Protection module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module allows you to embed CopySafe Video into web pages. It supports
both plain text and wysiwyg textareas.

Embedding is possible in three modes:


1. Demo Mode - displays a placeholder image.

2. Licensed Mode - displays encrypted Video and activates the browser plugin.

3. Debugging Mode - displays the object tag HTML in a text area form object.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED
------------

* The Copysafe Video Protector software can be obtained from www.artistscope.com


INSTALLATION
------------

 - Install the Devel module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
------------

1. Configure settings from /admin/config/copysafe-video/copysafe_video_settings
2. Upload your .class files from
   admin/config/copysafe-video/copysafe_video_protection
3. Enable CopySafeVideo filter for your text format.

Author/Maintainers
------------------

 - William Kent (ArtistScope) - https://www.drupal.org/u/artistscope
 - Azz-eddine BERRAMOU (berramou) - https://www.drupal.org/u/berramou
